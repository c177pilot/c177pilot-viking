import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot, QTimer
from math import floor
#from customProgressBar import customProgressBar
#import ui.customProgressBar
#from ui.dialog import Ui_Dialog
#import ui.mydialog
import src.dialog800x480

class AppDialog(QtWidgets.QDialog, src.dialog800x480.Ui_Dialog):

    #define a style library to set styles of objects based on state of the object
    styles = {"active" : "background-color:#3080FF; color:#FFFFFF;",
              "not_active" : "background-color:#25EDFF; color:#FFFFFF;",
              "selected" : "background-color:#29A038; color:#FFFFFF;",
              "tuner_select" : "background-color:#A0A0A0; color:#FFFFFF",
              "OM_active" : "background-color:#3399ff",
              "MM_active" : "background-color:#ffff66; color:0066ff",
              "IM_active" : "background-color:#ffffff; color:#000000",
              "MKR_not_active": "background-color:rgb(47, 47, 47); color:#FFFFFF",
              }

    def __init__(self, parent=None):
        super(AppDialog, self).__init__(parent)
        self.setupUi(self)

        self.timer_IndicatorUpdate = QTimer()
        self.timer_IndicatorUpdate.timeout.connect(self.updateIndicators)
        self.timer_IndicatorUpdate.start(1000)

        self._comTunerActive = 0
        self._comMonDialSelected = None
        self._comMonitorActive = {0 : False,
                                  1 : False,
                                  2 : False}

        self._COM_Stdby_Dispatch = {0 : self.pushButton_COM_Stdby_0,
                                    1 : self.pushButton_COM_Stdby_1,
                                    2 : self.pushButton_COM_Stdby_2}

        self._label_COM_Stdby_text_Dispatch = {0 : self.label_COM_Stdby_text_0,
                                                1 : self.label_COM_Stdby_text_1,
                                                2 : self.label_COM_Stdby_text_2}

        self._ComMon_Dispatch = {0: self.pushButton_ComMon_0,
                                 1: self.pushButton_ComMon_1,
                                 2: self.pushButton_ComMon_2}

        self._label_COM_Stby_RX_Dispatch = {0 : self.label_COM_Stby_RX_0,
                                      1 : self.label_COM_Stby_RX_1,
                                      2 : self.label_COM_Stby_RX_2}

        self._COM_Stdby_Dispatch[self._comTunerActive].setStyleSheet(self.styles["active"])
        self.label_NAV_Stdby_0.setStyleSheet(self.styles["active"])

        self.pushButton_COM_Stdby_0.index=0
        self.pushButton_COM_Stdby_1.index=1
        self.pushButton_COM_Stdby_2.index=2

        self.pushButton_ComMon_0.index=0
        self.pushButton_ComMon_1.index=1
        self.pushButton_ComMon_2.index=2

        #setup the timers used for modal logic
        self._tuner_button_timeout = 5000   #timeout value for the tuner pushbuttons (change mode of dial_COM_MHz)

        self.timer_COM_tuner = QTimer()
        self.timer_COM_tuner.timeout.connect(self.timer_COM_tuner_timeout)

        self.timer_NAV_tuner = QTimer()
        self.timer_NAV_tuner.timeout.connect(self.timer_NAV_tuner_timeout)

        #connect callbacks to the GUI controls
        self.dial_COM_MHz.valueChanged.connect(self.dial_COM_MHz_changed)
        self.dial_COM_KHz.valueChanged.connect(self.dial_COM_KHz_changed)
        self.pushButton_COM_tuner.clicked.connect(self.pushButton_COM_tuner_click)
        self.pushButton_COM_toggle.clicked.connect(self.pushButton_COM_toggle_click)

        self.dial_NAV_MHz.valueChanged.connect(self.dial_NAV_MHz_changed)
        self.dial_NAV_KHz.valueChanged.connect(self.dial_NAV_KHz_changed)
        self.pushButton_NAV_tuner.clicked.connect(self.pushButton_NAV_tuner_click)
        self.pushButton_NAV_toggle.clicked.connect(self.pushButton_NAV_toggle_click)

        self.pushButton_COM_Stdby_0.clicked.connect(self.pushButton_COM_Stdby_click)
        self.pushButton_COM_Stdby_1.clicked.connect(self.pushButton_COM_Stdby_click)
        self.pushButton_COM_Stdby_2.clicked.connect(self.pushButton_COM_Stdby_click)

        self.pushButton_ComMon_0.clicked.connect(self.pushButton_ComMon_click)
        self.pushButton_ComMon_1.clicked.connect(self.pushButton_ComMon_click)
        self.pushButton_ComMon_2.clicked.connect(self.pushButton_ComMon_click)

        self._select_COM_stby(None)
        self._select_COM_mon(None)

    @pyqtSlot()
    def dial_COM_MHz_changed(self):

        if self.timer_COM_tuner.isActive():
            self.timer_COM_tuner.start(self._tuner_button_timeout) #reset the alarm
            self._select_COM_stby(self.dial_COM_MHz.value() % 3) #hardcoded #
        else:
            self.updateCOM()

    @pyqtSlot()
    def dial_COM_KHz_changed(self):

        if self.timer_COM_tuner.isActive():
            self.timer_COM_tuner.start(self._tuner_button_timeout)
            #this is defeating the logic of deselecting the COM_MON channel if the same one is selected in the end
            self._select_COM_mon(self.dial_COM_KHz.value() % 3) #hardcoded #
        else:
            self.updateCOM()

    @pyqtSlot()
    def pushButton_COM_tuner_click(self):
        if self.timer_COM_tuner.isActive():
            self.timer_COM_tuner_timeout()  #return to normal ops if the timer is active
        else:
            self.timer_COM_tuner.start(self._tuner_button_timeout) #otherwise activate the timer
            self._select_COM_stby(self._comTunerActive) #update the GUI to show state change

    @pyqtSlot()
    def pushButton_COM_toggle_click(self):
        swapCOM = self._comTunerActive
        swapFreq = self.label_COM_Active_0.text()
        self.label_COM_Active_0.setText(self._COM_Stdby_Dispatch[swapCOM].text())
        self._COM_Stdby_Dispatch[swapCOM].setText(swapFreq)

    @pyqtSlot()
    def dial_NAV_MHz_changed(self):
        self.updateNAV()

    @pyqtSlot()
    def dial_NAV_KHz_changed(self):
        self.updateNAV()

    @pyqtSlot()
    def pushButton_NAV_tuner_click(self):
        self.timer_NAV_tuner.start(self._tuner_button_timeout)

    @pyqtSlot()
    def pushButton_NAV_toggle_click(self):
        swapFreq = self.label_NAV_Active_0.text()
        self.label_NAV_Active_0.setText(self.label_NAV_Stdby_0.text())
        self.label_NAV_Stdby_0.setText(swapFreq)

    @pyqtSlot()
    def pushButton_COM_Stdby_click(self, data=None):

        if not data:
            target = self.sender()
            index = target.index
            self._select_COM_stby(index)

    def _select_COM_stby(self, index):


        for k in self._COM_Stdby_Dispatch:
            self._COM_Stdby_Dispatch[k].setStyleSheet(self.styles["not_active"])
            self._label_COM_Stdby_text_Dispatch[k].setStyleSheet(self.styles["not_active"])

        if index is not None:
            self._comTunerActive = index

            if self.timer_COM_tuner.isActive():
                self._COM_Stdby_Dispatch[index].setStyleSheet(self.styles["tuner_select"])
                self._label_COM_Stdby_text_Dispatch[index].setStyleSheet(self.styles["tuner_select"])
            else:
                self._COM_Stdby_Dispatch[index].setStyleSheet(self.styles["active"])
                self._label_COM_Stdby_text_Dispatch[index].setStyleSheet(self.styles["active"])


    @pyqtSlot()
    def pushButton_ComMon_click(self, data=None):

        if data is None:
            target = self.sender()
            index = target.index
            self._select_COM_mon(index)

    def _select_COM_mon(self, index):

        if self.timer_COM_tuner.isActive():
            for k in self._comMonitorActive:
                if self._comMonitorActive[k]:
                    self._ComMon_Dispatch[k].setStyleSheet(self.styles["selected"])
                    self._label_COM_Stby_RX_Dispatch[k].setStyleSheet(self.styles["selected"])
                else:
                    self._ComMon_Dispatch[k].setStyleSheet(self.styles["not_active"])
                    self._label_COM_Stby_RX_Dispatch[k].setStyleSheet(self.styles["not_active"])

            self._ComMon_Dispatch[index].setStyleSheet(self.styles["tuner_select"])
            self._label_COM_Stby_RX_Dispatch[index].setStyleSheet(self.styles["tuner_select"])
            self._comMonDialSelected = index

        else:

            if index is not None:
                #check if the current COM Monitor is already active then deactivate it
                if self._comMonitorActive[index] is True:
                    toggle = True
                else:
                    toggle = False

                if toggle:
                    self._comMonitorActive[index] = False
                    self._ComMon_Dispatch[index].setStyleSheet(self.styles["not_active"])
                    self._label_COM_Stby_RX_Dispatch[index].setStyleSheet(self.styles["not_active"])
                else:
                    self._comMonitorActive[index]=True
                    self._ComMon_Dispatch[index].setStyleSheet(self.styles["selected"])
                    self._label_COM_Stby_RX_Dispatch[index].setStyleSheet(self.styles["selected"])

            else:
                #set everything false initially to force only on Monitor active at a time
                for k in self._comMonitorActive:
                    self._comMonitorActive[k] = False
                    self._ComMon_Dispatch[k].setStyleSheet(self.styles["not_active"])
                    self._label_COM_Stby_RX_Dispatch[k].setStyleSheet(self.styles["not_active"])

    @pyqtSlot()
    def timer_COM_tuner_timeout(self):
        self.timer_COM_tuner.stop()
        self._select_COM_stby(self._comTunerActive)
        self._select_COM_mon(self._comMonDialSelected)

    @pyqtSlot()
    def timer_NAV_tuner_timeout(self):
        self.timer_NAV_tuner.stop()

    def updateNAV(self):
        navFreq = "{:06.2f}".format(self.dial_NAV_MHz.value()+self.dial_NAV_KHz.value() / 20.0)
        self.label_NAV_Stdby_0.setText(navFreq)

    def updateCOM(self):
        comFreq_float = self.dial_COM_MHz.value()+self.trunc(self.dial_COM_KHz.value() / 40.0, 2)
        comFreq = "{:06.2f}".format(comFreq_float)
        self._COM_Stdby_Dispatch[self._comTunerActive].setText(comFreq)

    def trunc(self,num, digits):
        return floor(num*pow(10,digits))/(pow(10,digits))

    def updateIndicators(self):

        #unnecessary for production code. Only here to spoof actual activity on the GUI
        self.scroll_GS.setValue(self.oscillatingFunc(100,self.scroll_GS.value(),7))
        #self.scroll_LOC.setValue(self.oscillatingFunc(100,self.scroll_LOC.value(),3))
        #self.scroll_LOC.update_graphicsView(self.oscillatingFunc(100, self.scroll_LOC.value(), 3))

        if not (self.scroll_LOC.value() % 5):
            self.pushButton_MKR_0.setStyleSheet(self.styles["OM_active"])
        else:
            self.pushButton_MKR_0.setStyleSheet(self.styles["MKR_not_active"])

        if not (self.scroll_LOC.value() % 8):
            self.pushButton_MKR_1.setStyleSheet(self.styles["MM_active"])
        else:
            self.pushButton_MKR_1.setStyleSheet(self.styles["MKR_not_active"])

        if not (self.scroll_LOC.value() % 7):
            self.pushButton_MKR_2.setStyleSheet(self.styles["IM_active"])
        else:
            self.pushButton_MKR_2.setStyleSheet(self.styles["MKR_not_active"])


    def oscillatingFunc(self,bipol_range,val,increment):
        rtnVal = val + increment
        if rtnVal > bipol_range:
            rtnVal = -bipol_range
        if rtnVal < -bipol_range:
            rtnVal = bipol_range
        return rtnVal

def main():
    #every QT GUI needs 1 app
    app = QtWidgets.QApplication(sys.argv)

    gui = AppDialog()

    #now show the Dialog
    gui.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

