# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog800x480.ui'
#
# Created by: PyQt5 UI code generator 5.7.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        Dialog.resize(800, 480)
        # Need to setup a frameless window so Titlebar does not show
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)

        #-------------- Setup up COM Frequency labels -----------------

        #This is the label that contains frequency that is doing TX/RX
        font = QtGui.QFont()
        font.setPointSize(50)
        font.setBold(True)
        font.setWeight(75)

        self.label_COM_Active_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_0.setGeometry(QtCore.QRect(0, 0, 200, 120))
        self.label_COM_Active_0.setFont(font)
        self.label_COM_Active_0.setAutoFillBackground(False)
        self.label_COM_Active_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_COM_Active_0.setLineWidth(2)
        self.label_COM_Active_0.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_COM_Active_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_0.setObjectName("label_COM_Active_0")

        #This is button that holds the frequency text for COM Standby #1
        font = QtGui.QFont()
        font.setPointSize(50)
        font.setBold(True)
        font.setWeight(75)

        self.pushButton_COM_Stdby_0 = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_Stdby_0.setGeometry(QtCore.QRect(0, 120, 200, 120))
        self.pushButton_COM_Stdby_0.setFont(font)
        self.pushButton_COM_Stdby_0.setDefault(False)
        self.pushButton_COM_Stdby_0.setFlat(False)
        self.pushButton_COM_Stdby_0.setObjectName("pushButton_COM_Stdby_0")

        #This is button that holds the frequency text for COM Standby #2
        self.pushButton_COM_Stdby_1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_Stdby_1.setGeometry(QtCore.QRect(0, 240, 200, 120))
        self.pushButton_COM_Stdby_1.setFont(font)
        self.pushButton_COM_Stdby_1.setFlat(False)
        self.pushButton_COM_Stdby_1.setObjectName("pushButton_COM_Stdby_1")

        #This is button that holds the frequency text for COM Standby #3
        self.pushButton_COM_Stdby_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_Stdby_2.setGeometry(QtCore.QRect(0, 360, 200, 120))
        self.pushButton_COM_Stdby_2.setFont(font)
        self.pushButton_COM_Stdby_2.setFlat(False)
        self.pushButton_COM_Stdby_2.setObjectName("pushButton_COM_Stdby_2")

        #------------- Setup COM monitoring pushbuttons -------------------

        #This is a static label that indicates which channel is the main TX/RX channel
        font = QtGui.QFont()
        font.setPointSize(45)
        font.setBold(True)
        font.setWeight(75)

        self.label_COM = QtWidgets.QLabel(Dialog)
        self.label_COM.setGeometry(QtCore.QRect(200, 0, 126, 120))
        self.label_COM.setFont(font)
        self.label_COM.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.label_COM.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_COM.setMidLineWidth(0)
        self.label_COM.setTextFormat(QtCore.Qt.AutoText)
        self.label_COM.setScaledContents(False)
        self.label_COM.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM.setObjectName("label_COM")

        font = QtGui.QFont()
        font.setPointSize(45)
        font.setBold(False)
        font.setWeight(50)

        #This is the pushbutton that activates/deactivates monitoring of the first standby channel
        self.pushButton_ComMon_0 = QtWidgets.QPushButton(Dialog)
        self.pushButton_ComMon_0.setGeometry(QtCore.QRect(200, 120, 126, 120))
        self.pushButton_ComMon_0.setFont(font)
        self.pushButton_ComMon_0.setAutoRepeat(False)
        self.pushButton_ComMon_0.setFlat(False)
        self.pushButton_ComMon_0.setObjectName("pushButton_ComMon_0")

        #This is the pushbutton that activates/deactivates monitoring of the second standby channel
        self.pushButton_ComMon_1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_ComMon_1.setGeometry(QtCore.QRect(200, 240, 126, 120))
        self.pushButton_ComMon_1.setFont(font)
        self.pushButton_ComMon_1.setFlat(False)
        self.pushButton_ComMon_1.setObjectName("pushButton_ComMon_1")

        #This is the pushbutton that activates/deactivates monitoring of the third standby channel
        self.pushButton_ComMon_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_ComMon_2.setGeometry(QtCore.QRect(200, 360, 126, 120))
        self.pushButton_ComMon_2.setFont(font)
        self.pushButton_ComMon_2.setFlat(False)
        self.pushButton_ComMon_2.setObjectName("pushButton_ComMon_2")


        #------------------ Setup Active NAV frequency labels ---------------------
        #------------------ Note, Active NAV drives CDI in panel or LCD display
        font = QtGui.QFont()
        font.setPointSize(45)
        font.setBold(True)
        font.setWeight(75)

        #This is the frequency label for the Active NAV freq that is driving the CDI (GUI and Panel)
        self.label_NAV_Active_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_0.setGeometry(QtCore.QRect(600, 0, 200, 90))
        self.label_NAV_Active_0.setFont(font)
        self.label_NAV_Active_0.setAutoFillBackground(False)
        self.label_NAV_Active_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_0.setObjectName("label_NAV_Active_0")

        #This is the frequency label for the Standby NAV frequency
        self.label_NAV_Stdby_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stdby_0.setGeometry(QtCore.QRect(600, 300, 200, 120))
        self.label_NAV_Stdby_0.setFont(font)
        self.label_NAV_Stdby_0.setAutoFillBackground(False)
        self.label_NAV_Stdby_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Stdby_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Stdby_0.setObjectName("label_NAV_Stdby_0")

        #This is the label where the Morse code ID of the Active NAV frequency is displayed
        self.label_NAV_Active_ID_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_ID_0.setGeometry(QtCore.QRect(400, 0, 200, 120))
        self.label_NAV_Active_ID_0.setFont(font)
        self.label_NAV_Active_ID_0.setAutoFillBackground(False)
        self.label_NAV_Active_ID_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_ID_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_ID_0.setObjectName("label_NAV_Active_ID_0")

        # This is a static label that labels the OBS reading from the CDI (the radial you are referencing navigation from
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)

        self.label_NAV_OBS = QtWidgets.QLabel(Dialog)
        self.label_NAV_OBS.setGeometry(QtCore.QRect(400, 120, 100, 60))
        self.label_NAV_OBS.setFont(font)
        self.label_NAV_OBS.setAutoFillBackground(False)
        self.label_NAV_OBS.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_OBS.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_OBS.setObjectName("label_NAV_OBS")


        # This is the label that value of the OBS radial
        self.label_NAV_OBS_Val = QtWidgets.QLabel(Dialog)
        self.label_NAV_OBS_Val.setGeometry(QtCore.QRect(500, 120, 100, 60))
        self.label_NAV_OBS_Val.setFont(font)
        self.label_NAV_OBS_Val.setAutoFillBackground(False)
        self.label_NAV_OBS_Val.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_OBS_Val.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_OBS_Val.setObjectName("label_NAV_OBS_Val")

        #This is the label that indicates TO or FROM direction for referencing of the radial error
        self.label_NAV_OBS_Dir = QtWidgets.QLabel(Dialog)
        self.label_NAV_OBS_Dir.setGeometry(QtCore.QRect(600, 90, 100, 90))
        self.label_NAV_OBS_Dir.setFont(font)
        self.label_NAV_OBS_Dir.setAutoFillBackground(False)
        self.label_NAV_OBS_Dir.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_OBS_Dir.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_OBS_Dir.setObjectName("label_NAV_OBS_Dir")

        #This is the label where the radial and distance (DME) of the Active NAV frequency is displayed
        self.label_NAV_Active_Position_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_Position_0.setGeometry(QtCore.QRect(400, 180, 400, 60))
        self.label_NAV_Active_Position_0.setFont(font)
        self.label_NAV_Active_Position_0.setAutoFillBackground(False)
        self.label_NAV_Active_Position_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_Position_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_Position_0.setObjectName("label_NAV_Active_Position_0")


        #------------ Setup the Standby NAV controls ----------------------

        font = QtGui.QFont()
        font.setPointSize(45)
        font.setBold(True)
        font.setWeight(75)

        # This is the frequency label for the Standby NAV frequency
        self.label_NAV_Stdby_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stdby_0.setGeometry(QtCore.QRect(600, 300, 200, 120))
        self.label_NAV_Stdby_0.setFont(font)
        self.label_NAV_Stdby_0.setAutoFillBackground(False)
        self.label_NAV_Stdby_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Stdby_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Stdby_0.setObjectName("label_NAV_Stdby_0")

        #This is the label where the Morse code ID of the Standby NAV frequency is displayed
        self.label_NAV_Active_ID_1 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_ID_1.setGeometry(QtCore.QRect(400, 300, 200, 120))
        self.label_NAV_Active_ID_1.setFont(font)
        self.label_NAV_Active_ID_1.setAutoFillBackground(False)
        self.label_NAV_Active_ID_1.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_ID_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_ID_1.setObjectName("label_NAV_Active_ID_1")

        #This is the label where the radial and distance (DME) of the Standby NAV frequency is displayed
        self.label_NAV_Active_Position_1 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_Position_1.setGeometry(QtCore.QRect(400, 420, 400, 60))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Active_Position_1.setFont(font)
        self.label_NAV_Active_Position_1.setAutoFillBackground(False)
        self.label_NAV_Active_Position_1.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_Position_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_Position_1.setObjectName("label_NAV_Active_Position_1")


        #----------------- Setup graphics for LOC and GS needles -------------------

        #This is the scrollbar to represent LOC needle
        self.scroll_LOC = QtWidgets.QScrollBar(Dialog)
        self.scroll_LOC.setGeometry(QtCore.QRect(400, 240, 400, 60))
        self.scroll_LOC.setMinimum(-100)
        self.scroll_LOC.setMaximum(100)
        self.scroll_LOC.setProperty("value", -10)
        self.scroll_LOC.setOrientation(QtCore.Qt.Horizontal)
        self.scroll_LOC.setObjectName("scroll_LOC")

        #This is the scrollbar to represent the glideslope needle
        self.scroll_GS = QtWidgets.QScrollBar(Dialog)
        self.scroll_GS.setGeometry(QtCore.QRect(326, 0, 74, 480))
        self.scroll_GS.setMinimum(-100)
        self.scroll_GS.setMaximum(100)
        self.scroll_GS.setProperty("value", 10)
        self.scroll_GS.setOrientation(QtCore.Qt.Vertical)
        self.scroll_GS.setObjectName("scroll_GS")

        #------------------------ Setup Approach Marker Indicators ------------------------
        #This is the pushbutton to represent the OuterMarker indicator
        self.pushButton_MKR_0 = QtWidgets.QPushButton(Dialog)
        self.pushButton_MKR_0.setGeometry(QtCore.QRect(700, 90, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_MKR_0.setFont(font)
        self.pushButton_MKR_0.setStyleSheet("")
        self.pushButton_MKR_0.setObjectName("pushButton_MKR_0")

        #This is the pushbutton to represent the MiddleMarker indicator
        self.pushButton_MKR_1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_MKR_1.setGeometry(QtCore.QRect(760, 90, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_MKR_1.setFont(font)
        self.pushButton_MKR_1.setObjectName("pushButton_MKR_1")

        #This is the pushbutton to represent the InnerMarker indicator
        self.pushButton_MKR_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_MKR_2.setGeometry(QtCore.QRect(730, 140, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_MKR_2.setFont(font)
        self.pushButton_MKR_2.setObjectName("pushButton_MKR_2")

        #----------------- Setup tuning dials (this is just for mockup until real dials added) ---------------
        #This is the dial to tune COM MHz
        self.dial_COM_MHz = QtWidgets.QDial(Dialog)
        self.dial_COM_MHz.setGeometry(QtCore.QRect(10, 470, 101, 91))
        self.dial_COM_MHz.setMinimum(118)
        self.dial_COM_MHz.setMaximum(136)
        self.dial_COM_MHz.setWrapping(True)
        self.dial_COM_MHz.setObjectName("dial_COM_MHz")

        #This is the dial to tune COM KHz
        self.dial_COM_KHz = QtWidgets.QDial(Dialog)
        self.dial_COM_KHz.setGeometry(QtCore.QRect(36, 484, 50, 64))
        self.dial_COM_KHz.setMaximum(40)
        self.dial_COM_KHz.setSingleStep(1)
        self.dial_COM_KHz.setPageStep(1)
        self.dial_COM_KHz.setProperty("value", 20)
        self.dial_COM_KHz.setWrapping(True)
        self.dial_COM_KHz.setObjectName("dial_COM_KHz")

        #This is the dial that tunes NAV KHz
        self.dial_NAV_KHz = QtWidgets.QDial(Dialog)
        self.dial_NAV_KHz.setGeometry(QtCore.QRect(726, 484, 50, 64))
        self.dial_NAV_KHz.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.dial_NAV_KHz.setStatusTip("")
        self.dial_NAV_KHz.setMaximum(20)
        self.dial_NAV_KHz.setSingleStep(1)
        self.dial_NAV_KHz.setPageStep(1)
        self.dial_NAV_KHz.setProperty("value", 10)
        self.dial_NAV_KHz.setSliderPosition(10)
        self.dial_NAV_KHz.setTracking(True)
        self.dial_NAV_KHz.setWrapping(True)
        self.dial_NAV_KHz.setObjectName("dial_NAV_KHz")

        #This is the dial that tunes NAV MHz
        self.dial_NAV_MHz = QtWidgets.QDial(Dialog)
        self.dial_NAV_MHz.setGeometry(QtCore.QRect(700, 470, 101, 91))
        self.dial_NAV_MHz.setMinimum(108)
        self.dial_NAV_MHz.setMaximum(117)
        self.dial_NAV_MHz.setWrapping(True)
        self.dial_NAV_MHz.setObjectName("dial_NAV_MHz")

        #----------------- Setup pushbuttons (this is just for mockup until real buttons are added) ---------------

        #This is the pushbutton to toggle COM frequency
        self.pushButton_COM_toggle = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_toggle.setGeometry(QtCore.QRect(110, 500, 51, 32))
        self.pushButton_COM_toggle.setObjectName("pushButton_COM_toggle")

        #This button selects a COM tuner (channel). It will be the center pushbutton on the inner encoder dial
        self.pushButton_COM_tuner = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_tuner.setGeometry(QtCore.QRect(40, 560, 36, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_COM_tuner.setFont(font)
        self.pushButton_COM_tuner.setFlat(True)
        self.pushButton_COM_tuner.setObjectName("pushButton_COM_tuner")

        #This is the button that toggles the active/standby NAV frequency
        self.pushButton_NAV_toggle = QtWidgets.QPushButton(Dialog)
        self.pushButton_NAV_toggle.setGeometry(QtCore.QRect(660, 500, 51, 32))
        self.pushButton_NAV_toggle.setObjectName("pushButton_NAV_toggle")

        #This button selects a NAV tuner (channel). It will be the center pushbutton on the inner encoder dial
        self.pushButton_NAV_tuner = QtWidgets.QPushButton(Dialog)
        self.pushButton_NAV_tuner.setGeometry(QtCore.QRect(730, 560, 36, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_NAV_tuner.setFont(font)
        self.pushButton_NAV_tuner.setFlat(True)
        self.pushButton_NAV_tuner.setObjectName("pushButton_NAV_tuner")

        #-------------- Setup text labels in the Freq fields to display text lables of likely frequency source -------

        #This is text that will indicate a communications facility based on GPS location and a database of frequencies
        self.label_COM_Active_text_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_text_0.setGeometry(QtCore.QRect(2, 90, 196, 28))
        self.label_COM_Active_text_0.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_COM_Active_text_0.setScaledContents(False)
        self.label_COM_Active_text_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_text_0.setObjectName("label_COM_Active_text_0")

        #This is text that will indicate a communications facility based on GPS location and a database of frequencies
        self.label_COM_Stdby_text_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stdby_text_0.setGeometry(QtCore.QRect(2, 210, 196, 28))
        self.label_COM_Stdby_text_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stdby_text_0.setObjectName("label_COM_Stdby_text_0")
        self.label_COM_Stdby_text_1 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stdby_text_1.setGeometry(QtCore.QRect(2, 330, 196, 28))

        #This is text that will label the active COM frequency based on GPS location and a database of frequencies
        self.label_COM_Stdby_text_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stdby_text_1.setObjectName("label_COM_Stdby_text_1")
        self.label_COM_Stdby_text_2 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stdby_text_2.setGeometry(QtCore.QRect(2, 450, 196, 28))
        self.label_COM_Stdby_text_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stdby_text_2.setObjectName("label_COM_Stdby_text_2")

        #This is text that will label the active NAV frequency based on GPS location and a database of frequencies
        self.label_NAV_Active_text = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_text.setGeometry(QtCore.QRect(400, 90, 200, 30))
        self.label_NAV_Active_text.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_text.setObjectName("label_NAV_Active_text")

        #This is text that can be diplayed below the Standby NAV Identifier
        self.label_NAV_Stby_text_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stby_text_0.setGeometry(QtCore.QRect(402, 390, 196, 30))
        self.label_NAV_Stby_text_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Stby_text_0.setObjectName("label_NAV_Stby_text_0")

        #This is text that can be diplayed below the Standby NAV Frequency
        self.label_NAV_Stby_text_1 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stby_text_1.setGeometry(QtCore.QRect(602, 390, 196, 30))
        self.label_NAV_Stby_text_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Stby_text_1.setObjectName("label_NAV_Stby_text_1")

        #--------- Setup TX/RX activity indicator lights ------------------

        #This is an indicator that lights up when active transmission is occuring
        self.label_COM_Active_TX = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_TX.setGeometry(QtCore.QRect(200, 90, 63, 30))
        self.label_COM_Active_TX.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_TX.setObjectName("label_COM_Active_TX")

        #This is an indicator that lights up when active receive is occuring on the active COM channel
        self.label_COM_Active_RX = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_RX.setGeometry(QtCore.QRect(263, 90, 63, 30))
        self.label_COM_Active_RX.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_RX.setObjectName("label_COM_Active_RX")

        #This is an indicator that lights up when active receive is occuring on the standby 1 COM channel
        self.label_COM_Stby_RX_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stby_RX_0.setGeometry(QtCore.QRect(202, 210, 122, 28))
        self.label_COM_Stby_RX_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stby_RX_0.setObjectName("label_COM_Stby_RX_0")

        #This is an indicator that lights up when active receive is occuring on the standby 2 COM channel
        self.label_COM_Stby_RX_1 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stby_RX_1.setGeometry(QtCore.QRect(202, 330, 122, 28))
        self.label_COM_Stby_RX_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stby_RX_1.setObjectName("label_COM_Stby_RX_1")

        #This is an indicator that lights up when active receive is occuring on the standby 3 COM channel
        self.label_COM_Stby_RX_2 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stby_RX_2.setGeometry(QtCore.QRect(202, 450, 122, 28))
        self.label_COM_Stby_RX_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stby_RX_2.setObjectName("label_COM_Stby_RX_2")

#Setup (Raise) all controls to be visible on the form
        self.dial_NAV_MHz.raise_()
        self.label_COM_Active_0.raise_()
        self.pushButton_ComMon_0.raise_()
        self.pushButton_ComMon_1.raise_()
        self.pushButton_ComMon_2.raise_()
        self.label_NAV_Active_0.raise_()
        self.label_NAV_Active_ID_0.raise_()
        self.label_NAV_Active_Position_0.raise_()
        self.label_NAV_OBS.raise_()
        self.label_NAV_OBS_Val.raise_()
        self.label_NAV_OBS_Dir.raise_()
        self.label_NAV_Active_Position_1.raise_()
        self.label_NAV_Active_ID_1.raise_()
        self.label_NAV_Stdby_0.raise_()
        self.scroll_LOC.raise_()
        self.pushButton_MKR_0.raise_()
        self.pushButton_MKR_1.raise_()
        self.pushButton_MKR_2.raise_()
        self.scroll_GS.raise_()
        self.dial_COM_MHz.raise_()
        self.dial_COM_KHz.raise_()
        self.pushButton_COM_toggle.raise_()
        self.pushButton_COM_Stdby_0.raise_()
        self.pushButton_COM_tuner.raise_()
        self.dial_NAV_KHz.raise_()
        self.pushButton_NAV_toggle.raise_()
        self.pushButton_NAV_tuner.raise_()
        self.pushButton_COM_Stdby_1.raise_()
        self.pushButton_COM_Stdby_2.raise_()
        self.label_COM.raise_()
        self.label_COM_Active_text_0.raise_()
        self.label_COM_Stdby_text_0.raise_()
        self.label_COM_Stdby_text_1.raise_()
        self.label_COM_Stdby_text_2.raise_()
        self.label_COM_Active_TX.raise_()
        self.label_COM_Active_RX.raise_()
        self.label_COM_Stby_RX_0.raise_()
        self.label_COM_Stby_RX_1.raise_()
        self.label_COM_Stby_RX_2.raise_()
        self.label_NAV_Active_text.raise_()
        self.label_NAV_Stby_text_0.raise_()
        self.label_NAV_Stby_text_1.raise_()

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        Dialog.setStyleSheet(".QMainWindow{\n"
                             "background-color:#1d1d1d;\n"
                             "}\n"
                             "\n"
                             "QDialog{\n"
                             "background-color:rgb(24, 24, 24);\n"
                             "}\n"
                             "\n"
                             "QLabel{\n"
                             "background-color:rgb(41, 160, 56);\n"
                             "color:rgb(255,255,255);\n"
                             "}\n"
                             "\n"
                             "Qlabel#label_COM_Stby_text_0 {\n"
                             "border-style:solid;\n"
                             "background-color:rgb(37, 237, 255);\n"
                             "color:#fff;\n"
                             "border: 2px solid rgb(251, 255, 255);\n"
                             "}\n"
                             "\n"
                             "Qlabel#label_COM_Stby_text_1 {\n"
                             "border-style:solid;\n"
                             "background-color:rgb(37, 237, 255);\n"
                             "color:#fff;\n"
                             "border: 2px solid rgb(251, 255, 255);\n"
                             "}\n"
                             "\n"
                             "Qlabel#label_COM_Stby_text_2 {\n"
                             "border-style:solid;\n"
                             "background-color:rgb(37, 237, 255);\n"
                             "color:#fff;\n"
                             "border: 2px solid rgb(251, 255, 255);\n"
                             "}\n"
                             "\n"
                             "/*\n"
                             "QLabel#label_COM_Active_0{\n"
                             "background-color:rgb(41, 160, 56);\n"
                             "color:rgb(255,255,255);\n"
                             "}\n"
                             "*/\n"
                             "\n"
                             "QPushButton{\n"
                             "border-style:solid;\n"
                             "background-color:rgb(37, 237, 255);\n"
                             "color:#fff;\n"
                             "/*border-radius:4px;*/\n"
                             "border: 2px solid rgb(251, 255, 255);\n"
                             "}\n"
                             "\n"
                             "#pushButton_MKR_0 {background-color: rgb(47, 47, 47)}\n"
                             "#pushButton_MKR_1 {background-color: rgb(47, 47, 47)}\n"
                             "#pushButton_MKR_2 {background-color: rgb(47, 47, 47)}\n"
                             "#pushButton_NAV_toggle {background-color: rgb(47, 47, 47)}\n"
                             "#pushButton_COM_toggle {background-color: rgb(47, 47, 47)}\n"
                             "#pushButton_NAV_tuner {background-color: rgb(47, 47, 47)}\n"
                             "#pushButton_COM_tuner {background-color: rgb(47, 47, 47)}\n"
                             "\n"
                             "QDial{background-color: rgb(47, 47, 47)}\n"
                             "\n"
                             "/*\n"
                             "QPushButton:hover{\n"
                             "color:#ccc;\n"
                             "background-color: qlineargradient(spread:pad, x1:0.517, y1:0, x2:0.517, y2:1, stop:0 rgba(45, 45, 45, 255), stop:0.505682 rgba(45, 45, 45, 255), stop:1 rgba(29, 29, 29, 255));\n"
                             "border-color:#2d89ef;\n"
                             "border-width:2px;\n"
                             "}\n"
                             "*/\n"
                             "\n"
                             "QPushButton:pressed{\n"
                             "background-color: qlineargradient(spread:pad, x1:0.517, y1:0, x2:0.517, y2:1, stop:0 rgba(29, 29, 29, 255), stop:0.505682 rgba(45, 45, 45, 255), stop:1 rgba(29, 29, 29, 255));\n"
                             "}\n"
                             "\n"
                             " QScrollBar:vertical {\n"
                             "     /*border: 2px solid grey;\n"
                             "     background: rgb(41, 160, 56);\n"
                             "     width: 15px;\n"
                             "     margin: 22px 0 22px 0; */\n"
                             " }\n"
                             "\n"
                             "QScrollBar:horizontal {\n"
                             "    /* border: 2px solid grey;\n"
                             "     background: rgb(41, 160, 56);\n"
                             "     width: 15px;\n"
                             "     margin: 22px 0 22px 0; */\n"
                             "}\n"
                             "\n"
                             "QScrollBar::handle:vertical {\n"
                             "     background: white;\n"
                             "     min-height: 20px;\n"
                             " }\n"
                             "\n"
                             "QScrollBar::handle:horizontal {\n"
                             "    background: white;\n"
                             "    min-width: 20px;\n"
                             "}\n"
                             "\n"
                             " /*\n"
                             " QScrollBar::add-line:vertical {\n"
                             "     border: 2px solid grey;\n"
                             "     background: #32CC99;\n"
                             "     height: 20px;\n"
                             "     subcontrol-position: bottom;\n"
                             "     subcontrol-origin: margin;\n"
                             " }\n"
                             "\n"
                             " QScrollBar::sub-line:vertical {\n"
                             "     border: 2px solid grey;\n"
                             "     background: #32CC99;\n"
                             "     height: 15px;\n"
                             "     subcontrol-position: top;\n"
                             "     subcontrol-origin: margin;\n"
                             " }\n"
                             " QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {\n"
                             "     border: 2px solid grey;\n"
                             "     width: 3px;\n"
                             "     height: 3px;\n"
                             "     background: white;\n"
                             " }\n"
                             "\n"
                             " QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {\n"
                             "     background: none;\n"
                             " }\n"
                             "*/\n"
                             "\n"
                             "\n"
                             "\n"
                             "/*\n"
                             "\n"
                             "\n"
                             "QScrollBar::add-line:horizontal {\n"
                             "    background: blue;\n"
                             "    width: 16px;\n"
                             "    subcontrol-position: right;\n"
                             "    subcontrol-origin: margin;\n"
                             "    border: 2px solid black;\n"
                             "}\n"
                             "\n"
                             "QScrollBar::sub-line:horizontal {\n"
                             "    background: magenta;\n"
                             "    width: 16px;\n"
                             "    subcontrol-position: top right;\n"
                             "    subcontrol-origin: margin;\n"
                             "    border: 2px solid black;\n"
                             "    position: absolute;\n"
                             "    right: 20px;\n"
                             "}\n"
                             "\n"
                             "QScrollBar:left-arrow:horizontal, QScrollBar::right-arrow:horizontal {\n"
                             "    width: 3px;\n"
                             "    height: 3px;\n"
                             "    background: pink;\n"
                             "}\n"
                             "\n"
                             "QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {\n"
                             "    background: none;\n"
                             "}\n"
                             "*/\n"
                             "\n"
                             "/*\n"
                             "QMenuBar{\n"
                             "background-color:#1d1d1d;\n"
                             "padding:5px;\n"
                             "    font: 12pt \"MS Shell Dlg 2\";\n"
                             "}\n"
                             "\n"
                             "QMenuBar::item{\n"
                             "background-color:#1d1d1d;\n"
                             "color:#fff;\n"
                             "padding:5px;\n"
                             "\n"
                             "}\n"
                             "\n"
                             "QMenu{\n"
                             "color:#fff;\n"
                             "padding:0;\n"
                             "}\n"
                             "\n"
                             "QMenu::item:selected{\n"
                             "color:#fff;\n"
                             "background-color:#00aba9;\n"
                             "}\n"
                             "\n"
                             "QTableWidget{\n"
                             "background-color:#3d3d3d;\n"
                             "color:#fff;\n"
                             "  selection-background-color: #da532c;\n"
                             "border:solid;\n"
                             "border-width:3px;\n"
                             "border-color:#da532c;\n"
                             "}\n"
                             "\n"
                             "QHeaderView::section{\n"
                             "background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(20, 158, 217, 255), stop:1 rgba(36, 158, 217, 255));\n"
                             "border:none;\n"
                             "border-top-style:solid;\n"
                             "border-width:1px;\n"
                             "border-top-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(20, 158, 217, 255), stop:1 rgba(36, 158, 217, 255));\n"
                             "color:#fff;\n"
                             "\n"
                             "}\n"
                             "\n"
                             "QHeaderView{\n"
                             "background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(20, 158, 217, 255), stop:1 rgba(36, 158, 217, 255));\n"
                             "\n"
                             "border:none;\n"
                             "border-top-style:solid;\n"
                             "border-width:1px;\n"
                             "border-top-color:#149ED9;\n"
                             "color:#fff;\n"
                             "    font: 75 12pt \"Calibri\";\n"
                             "}\n"
                             "\n"
                             "QTableCornerButton::section{\n"
                             "border:none;\n"
                             "background-color:#149ED9;\n"
                             "}\n"
                             "\n"
                             "QListWidget{\n"
                             "background-color:#3d3d3d;\n"
                             "color:#fff;\n"
                             "}\n"
                             "\n"
                             "QMenu{\n"
                             "background-color:#3d3d3d;\n"
                             "}\n"
                             "QStatusBar{\n"
                             "background-color:#7e3878;\n"
                             "color:#fff;\n"
                             "}\n"
                             "\n"
                             "\n"
                             "\n"
                             "QTabWidget::tab{\n"
                             "background-color:#3d3d3d;\n"
                             "}\n"
                             "\n"
                             "QLineEdit{\n"
                             "border-radius:0;\n"
                             "}\n"
                             "\n"
                             "QProgressBar{\n"
                             "border-radius:0;\n"
                             "text-align:center;\n"
                             "color:#fff;\n"
                             "background-color:transparent;\n"
                             "border: 2px solid #e3a21a;\n"
                             "border-radius:7px;\n"
                             "    font: 75 12pt \"Open Sans\";\n"
                             "\n"
                             "}\n"
                             "\n"
                             "QProgressBar::chunk{\n"
                             "background-color:#2d89ef;\n"
                             "width:20px;\n"
                             "}\n"
                             "\n"
                             "*/")

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Viking1"))
        self.label_COM_Active_0.setText(_translate("Dialog", "124.175"))
        self.pushButton_ComMon_0.setText(_translate("Dialog", "MON"))
        self.pushButton_ComMon_1.setText(_translate("Dialog", "MON"))
        self.pushButton_ComMon_2.setText(_translate("Dialog", "MON"))
        self.label_NAV_Active_0.setText(_translate("Dialog", "109.6"))
        self.label_NAV_Active_ID_0.setText(_translate("Dialog", "AHN"))
        self.label_NAV_Active_Position_0.setText(_translate("Dialog", "r280/32.3nm"))
        self.label_NAV_OBS.setText(_translate("Dialog", "OBS"))
        self.label_NAV_OBS_Val.setText(_translate("Dialog", "275"))
        self.label_NAV_OBS_Dir.setText(_translate("Dialog", "FRM"))
        self.label_NAV_Active_Position_1.setText(_translate("Dialog", "r187/59.5nm"))
        self.label_NAV_Active_ID_1.setText(_translate("Dialog", "HRS"))
        self.label_NAV_Stdby_0.setText(_translate("Dialog", "109.8"))
        self.pushButton_MKR_0.setText(_translate("Dialog", "OM"))
        self.pushButton_MKR_1.setText(_translate("Dialog", "MM"))
        self.pushButton_MKR_2.setText(_translate("Dialog", "IM"))
        self.pushButton_COM_toggle.setText(_translate("Dialog", "<->"))
        self.pushButton_COM_Stdby_0.setText(_translate("Dialog", "126.97"))
        self.pushButton_COM_tuner.setText(_translate("Dialog", "Push"))
        self.dial_NAV_KHz.setToolTip(_translate("Dialog", "NAV KHz"))
        self.pushButton_NAV_toggle.setText(_translate("Dialog", "<->"))
        self.pushButton_NAV_tuner.setText(_translate("Dialog", "Push"))
        self.pushButton_COM_Stdby_1.setText(_translate("Dialog", "132.27"))
        self.pushButton_COM_Stdby_2.setText(_translate("Dialog", "121.8"))
        self.label_COM.setText(_translate("Dialog", "COM"))
        self.label_COM_Active_text_0.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Stdby_text_0.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Stdby_text_1.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Stdby_text_2.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Active_TX.setText(_translate("Dialog", "TX"))
        self.label_COM_Active_RX.setText(_translate("Dialog", "RX"))
        self.label_COM_Stby_RX_0.setText(_translate("Dialog", "RX"))
        self.label_COM_Stby_RX_1.setText(_translate("Dialog", "RX"))
        self.label_COM_Stby_RX_2.setText(_translate("Dialog", "RX"))
        self.label_NAV_Active_text.setText(_translate("Dialog", "TextLabel"))
        self.label_NAV_Stby_text_0.setText(_translate("Dialog", "TextLabel"))
        self.label_NAV_Stby_text_1.setText(_translate("Dialog", "TextLabel"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

