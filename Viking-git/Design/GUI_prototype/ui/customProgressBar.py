from PyQt5 import QtCore, QtGui, QtWidgets

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class customProgressBar(QtWidgets.QGraphicsView):
    def __init__(self, parent=None, xdim=100, ydim=20,incr=0.0):
        QtWidgets.QWidget.__init__(self, parent)
        self.parent = parent
        self.incr = incr
        self.RectY=ydim
        self.RectX=xdim
        self.setupUi()

    def setupUi(self):
        # self.RectX = 400 #515.0  #### Graphics width 2 inches
        # self.RectY = 60.0  #### Graphics height 0.3125 inches

        self.setObjectName(_fromUtf8("graphicsView"))
        self.setMaximumSize(int(self.RectX+7), int(self.RectY+5))  ##### Widget size with 0.2 % margins
        self.setMinimumSize(int(self.RectX+7), int(self.RectY+5))

        self.borderColor = QtGui.QColor(0, 0, 255)  #### Border color
        self.backgroundColor = QtGui.QColor(0, 255, 0)  #### Background color
        self.tickColor = QtGui.QColor(255, 255, 255)  #### Tick colors


        #self.pointerPath = r'C:\Users\Pankaj-Work\Desktop\cursor.png'  #### Pointer image path
        self.pointerPath = r'cursor.png'  #### Pointer image path

        #### Please change pointer image path as per your location
        #### Change color of pointer in image to change pointer color in widget

        self.scene = QtWidgets.QGraphicsScene()
        self.setup_GraphicsView()
        self.update_graphicsView(self.incr)

    def addBorders(self):
        self.gradientRect = QtWidgets.QGraphicsRectItem(0, 0, self.RectX, self.RectY)
        self.scene.addItem(self.gradientRect)
        self.borderRect = QtCore.QRectF(0, 0, self.RectX, self.RectY)
        self.scene.addRect(self.borderRect, self.borderColor)
        self.gradBrush = QtGui.QBrush(self.backgroundColor)
        self.gradientRect.setBrush(self.gradBrush)

    def setup_GraphicsView(self):
        self.scene.setSceneRect(0, 0, self.RectX, self.RectY)
        self.addBorders()
        self.setScene(self.scene)

        #setup the pointer as a pixel map
        self.pixmap = QtGui.QPixmap(self.pointerPath)
        self.pixMapItem = QtWidgets.QGraphicsPixmapItem(self.pixmap)

        #the pointer graphic is 394 pixels wide, so 197 pixels is half width

        self.pixMapItem.setOffset(9.5 * 0.77 * (self.RectX), (self.RectY / 8.0))  #### Positioning pointer
        #self.pixMapItem.setScale(0.13)  #### Pointer scaling
        self.pixMapItem.setScale(self.RectY*0.13/85)  #### Pointer scaling

        self.scene.addItem(self.pixMapItem)

        self.pen = QtGui.QPen()

        self.pen.setColor(self.tickColor)

        for i in range(1, 10):
            self.tickHeight = self.RectY*3.0/85  #### Tick height
            self.pen.setWidth(5)  #### Tick width
            #self.pen.setWidth(self.tickHeight)

            if i == 5:
                self.tickHeight = self.RectY*10.0/85  #### Zero tick height
                self.pen.setWidth(3)  #### Zero tick width

            self.lineItem = QtWidgets.QGraphicsLineItem()
            self.lineItem.setToolTip(str(i))
            self.lineItem.setLine((i / 10.0) * self.RectX, (self.RectY / 2.0) - self.tickHeight,
                                  (i / 10.0) * self.RectX, (self.RectY / 2.0) + self.tickHeight)
            self.scene.addItem(self.lineItem)
            self.lineItem.setPen(self.pen)

    def value(self):
        return self.incr

    def update_graphicsView(self, incr=None):
        #### Updating pointer
        if incr == None:
            incr = 0.0
        setPos = ((incr + 100.0) / 20.0) - 0.5
        self.pixMapItem.setOffset(setPos * 0.77 * (self.RectX), (self.RectY / 8.0))
        self.incr = incr



