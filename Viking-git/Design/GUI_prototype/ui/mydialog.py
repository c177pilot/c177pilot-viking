# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog.ui'
#
# Created by: PyQt5 UI code generator 5.7.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from ui.customProgressBar import customProgressBar

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        #avGeom = QtWidgets.QDesktopWidget.availableGeometry()
        Dialog.resize(800, 580)
        Dialog.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        #Dialog.showFullScreen()
        Dialog.setStyleSheet("QMainWindow{\n"
"background-color:#1d1d1d;\n"
"}\n"
"\n"
"QDialog{\n"
"background-color:rgb(24, 24, 24);\n"
"}\n"
"\n"
"QLabel{\n"
"background-color:rgb(41, 160, 56);\n"
"color:rgb(255,255,255);\n"
"}\n"
"\n"
"QPushButton{\n"
"border-style:solid;\n"
"\n"
"background-color:rgb(37, 237, 255);\n"
"color:#fff;\n"
"/*border-radius:4px;*/\n"
"border: 2px solid rgb(251, 255, 255);\n"
"}\n"
"\n"
"#pushButton_MKR_0 {background-color: rgb(47, 47, 47)}\n"
"#pushButton_MKR_1 {background-color: rgb(47, 47, 47)}\n"
"#pushButton_MKR_2 {background-color: rgb(47, 47, 47)}\n"
"#pushButton_NAV_toggle {background-color: rgb(47, 47, 47)}\n"
"#pushButton_COM_toggle {background-color: rgb(47, 47, 47)}\n"
"#pushButton_NAV_tuner {background-color: rgb(47, 47, 47)}\n"
"#pushButton_COM_tuner {background-color: rgb(47, 47, 47)}\n"
"QDial{background-color: rgb(47, 47, 47)}\n"
"\n"
"/*\n"
"QPushButton:hover{\n"
"color:#ccc;\n"
"background-color: qlineargradient(spread:pad, x1:0.517, y1:0, x2:0.517, y2:1, stop:0 rgba(45, 45, 45, 255), stop:0.505682 rgba(45, 45, 45, 255), stop:1 rgba(29, 29, 29, 255));\n"
"border-color:#2d89ef;\n"
"border-width:2px;\n"
"}\n"
"*/\n"
"\n"
"QPushButton:pressed{\n"
"background-color: qlineargradient(spread:pad, x1:0.517, y1:0, x2:0.517, y2:1, stop:0 rgba(29, 29, 29, 255), stop:0.505682 rgba(45, 45, 45, 255), stop:1 rgba(29, 29, 29, 255));\n"
"}\n"
"\n"
" QScrollBar:vertical {\n"
"     border: 2px solid grey;\n"
"     background: rgb(41, 160, 56);\n"
"     /*width: 15px;\n"
"     margin: 22px 0 22px 0; */\n"
" }\n"
"\n"
"QScrollBar:horizontal {\n"
"     border: 2px solid grey;\n"
"     background: rgb(41, 160, 56);\n"
"     /*width: 15px;\n"
"     margin: 22px 0 22px 0; */\n"
"}\n"
"\n"
"QScrollBar::handle:vertical {\n"
"     background: white;\n"
"     min-height: 20px;\n"
" } \n"
"\n"
"QScrollBar::handle:horizontal {\n"
"    background: white;\n"
"    min-width: 20px;\n"
"}\n"
"\n"
" /*\n"
" QScrollBar::add-line:vertical {\n"
"     border: 2px solid grey;\n"
"     background: #32CC99;\n"
"     height: 20px;\n"
"     subcontrol-position: bottom;\n"
"     subcontrol-origin: margin;\n"
" }\n"
"\n"
" QScrollBar::sub-line:vertical {\n"
"     border: 2px solid grey;\n"
"     background: #32CC99;\n"
"     height: 15px;\n"
"     subcontrol-position: top;\n"
"     subcontrol-origin: margin;\n"
" }\n"
" QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {\n"
"     border: 2px solid grey;\n"
"     width: 3px;\n"
"     height: 3px;\n"
"     background: white;\n"
" }\n"
"\n"
" QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {\n"
"     background: none;\n"
" }\n"
"*/\n"
"\n"
"\n"
"\n"
"/*\n"
"\n"
"\n"
"QScrollBar::add-line:horizontal {\n"
"    background: blue;\n"
"    width: 16px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"    border: 2px solid black;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"    background: magenta;\n"
"    width: 16px;\n"
"    subcontrol-position: top right;\n"
"    subcontrol-origin: margin;\n"
"    border: 2px solid black;\n"
"    position: absolute;\n"
"    right: 20px;\n"
"}\n"
"\n"
"QScrollBar:left-arrow:horizontal, QScrollBar::right-arrow:horizontal {\n"
"    width: 3px;\n"
"    height: 3px;\n"
"    background: pink;\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {\n"
"    background: none;\n"
"}\n"
"*/\n"
"\n"
"/*\n"
"QMenuBar{\n"
"background-color:#1d1d1d;\n"
"padding:5px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"\n"
"QMenuBar::item{\n"
"background-color:#1d1d1d;\n"
"color:#fff;\n"
"padding:5px;\n"
"\n"
"}\n"
"\n"
"QMenu{\n"
"color:#fff;\n"
"padding:0;\n"
"}\n"
"\n"
"QMenu::item:selected{\n"
"color:#fff;\n"
"background-color:#00aba9;\n"
"}\n"
"\n"
"QTableWidget{\n"
"background-color:#3d3d3d;\n"
"color:#fff;\n"
"  selection-background-color: #da532c;\n"
"border:solid;\n"
"border-width:3px;\n"
"border-color:#da532c;\n"
"}\n"
"\n"
"QHeaderView::section{\n"
"background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(20, 158, 217, 255), stop:1 rgba(36, 158, 217, 255));\n"
"border:none;\n"
"border-top-style:solid;\n"
"border-width:1px;\n"
"border-top-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(20, 158, 217, 255), stop:1 rgba(36, 158, 217, 255));\n"
"color:#fff;\n"
"\n"
"}\n"
"\n"
"QHeaderView{\n"
"background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(20, 158, 217, 255), stop:1 rgba(36, 158, 217, 255));\n"
"\n"
"border:none;\n"
"border-top-style:solid;\n"
"border-width:1px;\n"
"border-top-color:#149ED9;\n"
"color:#fff;\n"
"    font: 75 12pt \"Calibri\";\n"
"}\n"
"\n"
"QTableCornerButton::section{\n"
"border:none;\n"
"background-color:#149ED9;\n"
"}\n"
"\n"
"QListWidget{\n"
"background-color:#3d3d3d;\n"
"color:#fff;\n"
"}\n"
"\n"
"QMenu{\n"
"background-color:#3d3d3d;\n"
"}\n"
"QStatusBar{\n"
"background-color:#7e3878;\n"
"color:#fff;\n"
"}\n"
"\n"
"\n"
"\n"
"QTabWidget::tab{\n"
"background-color:#3d3d3d;\n"
"}\n"
"\n"
"QLineEdit{\n"
"border-radius:0;\n"
"}\n"
"\n"
"QProgressBar{\n"
"border-radius:0;\n"
"text-align:center;\n"
"color:#fff;\n"
"background-color:transparent;\n"
"border: 2px solid #e3a21a;\n"
"border-radius:7px;\n"
"    font: 75 12pt \"Open Sans\";\n"
"\n"
"}\n"
"\n"
"QProgressBar::chunk{\n"
"background-color:#2d89ef;\n"
"width:20px;\n"
"}\n"
"\n"
"*/")
        self.label_COM_Active_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_0.setGeometry(QtCore.QRect(0, 0, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.label_COM_Active_0.setFont(font)
        self.label_COM_Active_0.setAutoFillBackground(False)
        self.label_COM_Active_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_COM_Active_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_0.setObjectName("label_COM_Active_0")
        self.pushButton_ComMon_0 = QtWidgets.QPushButton(Dialog)
        self.pushButton_ComMon_0.setGeometry(QtCore.QRect(200, 120, 126, 90))
        font = QtGui.QFont()
        font.setPointSize(26)
        self.pushButton_ComMon_0.setFont(font)
        self.pushButton_ComMon_0.setObjectName("pushButton_ComMon_0")
        self.pushButton_ComMon_1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_ComMon_1.setGeometry(QtCore.QRect(200, 240, 126, 90))
        font = QtGui.QFont()
        font.setPointSize(26)
        self.pushButton_ComMon_1.setFont(font)
        self.pushButton_ComMon_1.setObjectName("pushButton_ComMon_1")
        self.pushButton_ComMon_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_ComMon_2.setGeometry(QtCore.QRect(200, 360, 126, 90))
        font = QtGui.QFont()
        font.setPointSize(26)
        self.pushButton_ComMon_2.setFont(font)
        self.pushButton_ComMon_2.setObjectName("pushButton_ComMon_2")
        self.label_NAV_Active_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_0.setGeometry(QtCore.QRect(600, 0, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Active_0.setFont(font)
        self.label_NAV_Active_0.setAutoFillBackground(False)
        self.label_NAV_Active_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_0.setObjectName("label_NAV_Active_0")
        self.label_NAV_Active_ID_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_ID_0.setGeometry(QtCore.QRect(400, 0, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Active_ID_0.setFont(font)
        self.label_NAV_Active_ID_0.setAutoFillBackground(False)
        self.label_NAV_Active_ID_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_ID_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_ID_0.setObjectName("label_NAV_Active_ID_0")
        self.label_NAV_Active_Position_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_Position_0.setGeometry(QtCore.QRect(400, 180, 400, 60))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Active_Position_0.setFont(font)
        self.label_NAV_Active_Position_0.setAutoFillBackground(False)
        self.label_NAV_Active_Position_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_Position_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_Position_0.setObjectName("label_NAV_Active_Position_0")
        self.label_NAV_OBS = QtWidgets.QLabel(Dialog)
        self.label_NAV_OBS.setGeometry(QtCore.QRect(400, 120, 100, 60))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_OBS.setFont(font)
        self.label_NAV_OBS.setAutoFillBackground(False)
        self.label_NAV_OBS.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_OBS.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_OBS.setObjectName("label_NAV_OBS")
        self.label_NAV_OBS_Val = QtWidgets.QLabel(Dialog)
        self.label_NAV_OBS_Val.setGeometry(QtCore.QRect(500, 120, 100, 60))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_OBS_Val.setFont(font)
        self.label_NAV_OBS_Val.setAutoFillBackground(False)
        self.label_NAV_OBS_Val.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_OBS_Val.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_OBS_Val.setObjectName("label_NAV_OBS_Val")
        self.label_NAV_OBS_Dir = QtWidgets.QLabel(Dialog)
        self.label_NAV_OBS_Dir.setGeometry(QtCore.QRect(600, 90, 100, 90))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_OBS_Dir.setFont(font)
        self.label_NAV_OBS_Dir.setAutoFillBackground(False)
        self.label_NAV_OBS_Dir.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_OBS_Dir.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_OBS_Dir.setObjectName("label_NAV_OBS_Dir")
        self.label_NAV_Active_Position_1 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_Position_1.setGeometry(QtCore.QRect(400, 420, 400, 60))
        font = QtGui.QFont()
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Active_Position_1.setFont(font)
        self.label_NAV_Active_Position_1.setAutoFillBackground(False)
        self.label_NAV_Active_Position_1.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_Position_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_Position_1.setObjectName("label_NAV_Active_Position_1")
        self.label_NAV_Active_ID_1 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_ID_1.setGeometry(QtCore.QRect(400, 300, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Active_ID_1.setFont(font)
        self.label_NAV_Active_ID_1.setAutoFillBackground(False)
        self.label_NAV_Active_ID_1.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Active_ID_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Active_ID_1.setObjectName("label_NAV_Active_ID_1")
        self.label_NAV_Stdby_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stdby_0.setGeometry(QtCore.QRect(600, 300, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.label_NAV_Stdby_0.setFont(font)
        self.label_NAV_Stdby_0.setAutoFillBackground(False)
        self.label_NAV_Stdby_0.setFrameShape(QtWidgets.QFrame.Box)
        self.label_NAV_Stdby_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_NAV_Stdby_0.setObjectName("label_NAV_Stdby_0")
        self.scroll_LOC = customProgressBar(Dialog,xdim=400,ydim=60)
        #self.scroll_LOC = QtWidgets.QScrollBar(Dialog)
        self.scroll_LOC.setGeometry(QtCore.QRect(400, 240, 400, 60))
        #self.scroll_LOC.setMinimum(-100)
        #self.scroll_LOC.setMaximum(100)
        self.scroll_LOC.setProperty("value", -10)
        #self.scroll_LOC.setOrientation(QtCore.Qt.Horizontal)
        self.scroll_LOC.setObjectName("scroll_LOC")
        self.pushButton_MKR_0 = QtWidgets.QPushButton(Dialog)
        self.pushButton_MKR_0.setGeometry(QtCore.QRect(700, 90, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_MKR_0.setFont(font)
        self.pushButton_MKR_0.setStyleSheet("")
        self.pushButton_MKR_0.setObjectName("pushButton_MKR_0")
        self.pushButton_MKR_1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_MKR_1.setGeometry(QtCore.QRect(760, 90, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_MKR_1.setFont(font)
        self.pushButton_MKR_1.setObjectName("pushButton_MKR_1")
        self.pushButton_MKR_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_MKR_2.setGeometry(QtCore.QRect(730, 140, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_MKR_2.setFont(font)
        self.pushButton_MKR_2.setObjectName("pushButton_MKR_2")
        #self.scroll_GS = customProgressBar
        self.scroll_GS = QtWidgets.QScrollBar(Dialog)
        self.scroll_GS.setGeometry(QtCore.QRect(326, 0, 74, 480))
        self.scroll_GS.setMinimum(-100)
        self.scroll_GS.setMaximum(100)
        self.scroll_GS.setProperty("value", 10)
        self.scroll_GS.setOrientation(QtCore.Qt.Vertical)
        self.scroll_GS.setObjectName("scroll_GS")
        self.dial_COM_MHz = QtWidgets.QDial(Dialog)
        self.dial_COM_MHz.setGeometry(QtCore.QRect(10, 470, 101, 91))
        self.dial_COM_MHz.setMinimum(118)
        self.dial_COM_MHz.setMaximum(136)
        self.dial_COM_MHz.setWrapping(True)
        self.dial_COM_MHz.setObjectName("dial_COM_MHz")
        self.dial_COM_KHz = QtWidgets.QDial(Dialog)
        self.dial_COM_KHz.setGeometry(QtCore.QRect(36, 484, 50, 64))
        self.dial_COM_KHz.setMaximum(40)
        self.dial_COM_KHz.setSingleStep(1)
        self.dial_COM_KHz.setPageStep(1)
        self.dial_COM_KHz.setProperty("value", 20)
        self.dial_COM_KHz.setWrapping(True)
        self.dial_COM_KHz.setObjectName("dial_COM_KHz")
        self.pushButton_COM_toggle = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_toggle.setGeometry(QtCore.QRect(110, 500, 51, 32))
        self.pushButton_COM_toggle.setObjectName("pushButton_COM_toggle")
        self.pushButton_COM_Stdby_0 = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_Stdby_0.setGeometry(QtCore.QRect(0, 120, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(32)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_COM_Stdby_0.setFont(font)
        self.pushButton_COM_Stdby_0.setDefault(False)
        self.pushButton_COM_Stdby_0.setFlat(False)
        self.pushButton_COM_Stdby_0.setObjectName("pushButton_COM_Stdby_0")
        self.pushButton_COM_tuner = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_tuner.setGeometry(QtCore.QRect(40, 560, 36, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_COM_tuner.setFont(font)
        self.pushButton_COM_tuner.setFlat(True)
        self.pushButton_COM_tuner.setObjectName("pushButton_COM_tuner")
        self.dial_NAV_KHz = QtWidgets.QDial(Dialog)
        self.dial_NAV_KHz.setGeometry(QtCore.QRect(726, 484, 50, 64))
        self.dial_NAV_KHz.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.dial_NAV_KHz.setStatusTip("")
        self.dial_NAV_KHz.setMaximum(20)
        self.dial_NAV_KHz.setSingleStep(1)
        self.dial_NAV_KHz.setPageStep(1)
        self.dial_NAV_KHz.setProperty("value", 10)
        self.dial_NAV_KHz.setSliderPosition(10)
        self.dial_NAV_KHz.setTracking(True)
        self.dial_NAV_KHz.setWrapping(True)
        self.dial_NAV_KHz.setObjectName("dial_NAV_KHz")
        self.dial_NAV_MHz = QtWidgets.QDial(Dialog)
        self.dial_NAV_MHz.setGeometry(QtCore.QRect(700, 470, 101, 91))
        self.dial_NAV_MHz.setMinimum(108)
        self.dial_NAV_MHz.setMaximum(117)
        self.dial_NAV_MHz.setWrapping(True)
        self.dial_NAV_MHz.setObjectName("dial_NAV_MHz")
        self.pushButton_NAV_toggle = QtWidgets.QPushButton(Dialog)
        self.pushButton_NAV_toggle.setGeometry(QtCore.QRect(660, 500, 51, 32))
        self.pushButton_NAV_toggle.setObjectName("pushButton_NAV_toggle")
        self.pushButton_NAV_tuner = QtWidgets.QPushButton(Dialog)
        self.pushButton_NAV_tuner.setGeometry(QtCore.QRect(730, 560, 36, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_NAV_tuner.setFont(font)
        self.pushButton_NAV_tuner.setFlat(True)
        self.pushButton_NAV_tuner.setObjectName("pushButton_NAV_tuner")
        self.pushButton_COM_Stdby_1 = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_Stdby_1.setGeometry(QtCore.QRect(0, 240, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(32)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_COM_Stdby_1.setFont(font)
        self.pushButton_COM_Stdby_1.setObjectName("pushButton_COM_Stdby_1")
        self.pushButton_COM_Stdby_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_COM_Stdby_2.setGeometry(QtCore.QRect(0, 360, 200, 90))
        font = QtGui.QFont()
        font.setPointSize(32)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_COM_Stdby_2.setFont(font)
        self.pushButton_COM_Stdby_2.setObjectName("pushButton_COM_Stdby_2")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(200, 0, 126, 90))
        font = QtGui.QFont()
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.label.setFrameShape(QtWidgets.QFrame.Box)
        self.label.setMidLineWidth(0)
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setScaledContents(False)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_COM_Active_text_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_text_0.setGeometry(QtCore.QRect(0, 90, 200, 30))
        self.label_COM_Active_text_0.setObjectName("label_COM_Active_text_0")
        self.label_COM_Stdby_text_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stdby_text_0.setGeometry(QtCore.QRect(0, 210, 200, 30))
        self.label_COM_Stdby_text_0.setObjectName("label_COM_Stdby_text_0")
        self.label_COM_Stdby_text_1 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stdby_text_1.setGeometry(QtCore.QRect(0, 330, 200, 30))
        self.label_COM_Stdby_text_1.setObjectName("label_COM_Stdby_text_1")
        self.label_COM_Stdby_text_2 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stdby_text_2.setGeometry(QtCore.QRect(0, 450, 200, 30))
        self.label_COM_Stdby_text_2.setObjectName("label_COM_Stdby_text_2")
        self.label_COM_Active_TX = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_TX.setGeometry(QtCore.QRect(200, 90, 63, 30))
        self.label_COM_Active_TX.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_TX.setObjectName("label_COM_Active_TX")
        self.label_COM_Active_RX = QtWidgets.QLabel(Dialog)
        self.label_COM_Active_RX.setGeometry(QtCore.QRect(263, 90, 63, 30))
        self.label_COM_Active_RX.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Active_RX.setObjectName("label_COM_Active_RX")
        self.label_COM_Stby_RX_0 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stby_RX_0.setGeometry(QtCore.QRect(200, 210, 126, 30))
        self.label_COM_Stby_RX_0.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stby_RX_0.setObjectName("label_COM_Stby_RX_0")
        self.label_COM_Stby_RX_1 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stby_RX_1.setGeometry(QtCore.QRect(200, 330, 126, 30))
        self.label_COM_Stby_RX_1.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stby_RX_1.setObjectName("label_COM_Stby_RX_1")
        self.label_COM_Stby_RX_2 = QtWidgets.QLabel(Dialog)
        self.label_COM_Stby_RX_2.setGeometry(QtCore.QRect(200, 450, 126, 30))
        self.label_COM_Stby_RX_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_COM_Stby_RX_2.setObjectName("label_COM_Stby_RX_2")
        self.label_NAV_Active_text = QtWidgets.QLabel(Dialog)
        self.label_NAV_Active_text.setGeometry(QtCore.QRect(400, 90, 200, 30))
        self.label_NAV_Active_text.setObjectName("label_NAV_Active_text")
        self.label_NAV_Stby_text_0 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stby_text_0.setGeometry(QtCore.QRect(400, 390, 200, 30))
        self.label_NAV_Stby_text_0.setObjectName("label_NAV_Stby_text_0")
        self.label_NAV_Stby_text_1 = QtWidgets.QLabel(Dialog)
        self.label_NAV_Stby_text_1.setGeometry(QtCore.QRect(600, 390, 200, 30))
        self.label_NAV_Stby_text_1.setObjectName("label_NAV_Stby_text_1")
        self.dial_NAV_MHz.raise_()
        self.label_COM_Active_0.raise_()
        self.pushButton_ComMon_0.raise_()
        self.pushButton_ComMon_1.raise_()
        self.pushButton_ComMon_2.raise_()
        self.label_NAV_Active_0.raise_()
        self.label_NAV_Active_ID_0.raise_()
        self.label_NAV_Active_Position_0.raise_()
        self.label_NAV_OBS.raise_()
        self.label_NAV_OBS_Val.raise_()
        self.label_NAV_OBS_Dir.raise_()
        self.label_NAV_Active_Position_1.raise_()
        self.label_NAV_Active_ID_1.raise_()
        self.label_NAV_Stdby_0.raise_()
        self.scroll_LOC.raise_()
        self.pushButton_MKR_0.raise_()
        self.pushButton_MKR_1.raise_()
        self.pushButton_MKR_2.raise_()
        self.scroll_GS.raise_()
        self.dial_COM_MHz.raise_()
        self.dial_COM_KHz.raise_()
        self.pushButton_COM_toggle.raise_()
        self.pushButton_COM_Stdby_0.raise_()
        self.pushButton_COM_tuner.raise_()
        self.dial_NAV_KHz.raise_()
        self.pushButton_NAV_toggle.raise_()
        self.pushButton_NAV_tuner.raise_()
        self.pushButton_COM_Stdby_1.raise_()
        self.pushButton_COM_Stdby_2.raise_()
        self.label.raise_()
        self.label_COM_Active_text_0.raise_()
        self.label_COM_Stdby_text_0.raise_()
        self.label_COM_Stdby_text_1.raise_()
        self.label_COM_Stdby_text_2.raise_()
        self.label_COM_Active_TX.raise_()
        self.label_COM_Active_RX.raise_()
        self.label_COM_Stby_RX_0.raise_()
        self.label_COM_Stby_RX_1.raise_()
        self.label_COM_Stby_RX_2.raise_()
        self.label_NAV_Active_text.raise_()
        self.label_NAV_Stby_text_0.raise_()
        self.label_NAV_Stby_text_1.raise_()

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Viking1"))
        self.label_COM_Active_0.setText(_translate("Dialog", "124.10"))
        self.pushButton_ComMon_0.setText(_translate("Dialog", "MON"))
        self.pushButton_ComMon_1.setText(_translate("Dialog", "MON"))
        self.pushButton_ComMon_2.setText(_translate("Dialog", "MON"))
        self.label_NAV_Active_0.setText(_translate("Dialog", "109.6"))
        self.label_NAV_Active_ID_0.setText(_translate("Dialog", "AHN"))
        self.label_NAV_Active_Position_0.setText(_translate("Dialog", "r280/32.3nm"))
        self.label_NAV_OBS.setText(_translate("Dialog", "OBS"))
        self.label_NAV_OBS_Val.setText(_translate("Dialog", "275"))
        self.label_NAV_OBS_Dir.setText(_translate("Dialog", "FRM"))
        self.label_NAV_Active_Position_1.setText(_translate("Dialog", "r187/59.5nm"))
        self.label_NAV_Active_ID_1.setText(_translate("Dialog", "HRS"))
        self.label_NAV_Stdby_0.setText(_translate("Dialog", "109.8"))
        self.pushButton_MKR_0.setText(_translate("Dialog", "OM"))
        self.pushButton_MKR_1.setText(_translate("Dialog", "MM"))
        self.pushButton_MKR_2.setText(_translate("Dialog", "IM"))
        self.pushButton_COM_toggle.setText(_translate("Dialog", "<->"))
        self.pushButton_COM_Stdby_0.setText(_translate("Dialog", "126.97"))
        self.pushButton_COM_tuner.setText(_translate("Dialog", "Push"))
        self.dial_NAV_KHz.setToolTip(_translate("Dialog", "NAV KHz"))
        self.pushButton_NAV_toggle.setText(_translate("Dialog", "<->"))
        self.pushButton_NAV_tuner.setText(_translate("Dialog", "Push"))
        self.pushButton_COM_Stdby_1.setText(_translate("Dialog", "132.27"))
        self.pushButton_COM_Stdby_2.setText(_translate("Dialog", "121.8"))
        self.label.setText(_translate("Dialog", "COM"))
        self.label_COM_Active_text_0.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Stdby_text_0.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Stdby_text_1.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Stdby_text_2.setText(_translate("Dialog", "TextLabel"))
        self.label_COM_Active_TX.setText(_translate("Dialog", "TX"))
        self.label_COM_Active_RX.setText(_translate("Dialog", "RX"))
        self.label_COM_Stby_RX_0.setText(_translate("Dialog", "RX"))
        self.label_COM_Stby_RX_1.setText(_translate("Dialog", "RX"))
        self.label_COM_Stby_RX_2.setText(_translate("Dialog", "RX"))
        self.label_NAV_Active_text.setText(_translate("Dialog", "TextLabel"))
        self.label_NAV_Stby_text_0.setText(_translate("Dialog", "TextLabel"))
        self.label_NAV_Stby_text_1.setText(_translate("Dialog", "TextLabel"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())


