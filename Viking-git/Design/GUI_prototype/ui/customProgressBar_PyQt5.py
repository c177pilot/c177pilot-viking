# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'graphicsView.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class customProgressBar(QtWidgets.QGraphicsView):
    def __init__(self,parent=None,incr=0.0):
        QtWidgets.QWidget.__init__(self,parent)
        self.parent=parent
        self.incr=incr
        self.setupUi()
    
    
    def setupUi(self):
        self.setObjectName(_fromUtf8("graphicsView"))
        self.setMaximumSize(515,85) ##### Widget size with 0.2 % margins
        self.setMinimumSize(515,85)
        self.borderColor=QtGui.QColor(0, 0, 255)#### Border color
        self.backgroundColor=QtGui.QColor(0,255,0)#### Background color
        self.tickColor=QtGui.QColor(255,255,255)#### Tick colors                
        self.RectX = 508.0 #### Graphics width 2 inches
        self.RectY = 79.375 #### Graphics height 0.3125 inches
        
        self.pointerPath = r'C:\Users\Pankaj-Work\Desktop\cursor.png' #### Pointer image path
        
        #### Please change pointer image path as per your location
        #### Change color of pointer in image to change pointer color in widget
        
        self.scene = QtWidgets.QGraphicsScene()
        self.setup_GraphicsView()
        self.update_graphicsView(self.incr)
        
       
    
    def addBorders(self):
        self.gradientRect=QtWidgets.QGraphicsRectItem(0, 0,self.RectX, self.RectY)
        self.scene.addItem(self.gradientRect)
        self.borderRect = QtCore.QRectF(0, 0,self.RectX, self.RectY)
        self.scene.addRect(self.borderRect, self.borderColor) 
        self.gradBrush=QtGui.QBrush(self.backgroundColor) 
        self.gradientRect.setBrush(self.gradBrush)
                
    def setup_GraphicsView(self):
        self.scene.setSceneRect(0, 0, self.RectX, self.RectY)
        self.addBorders()
        self.setScene(self.scene)
        self.pixmap = QtGui.QPixmap(self.pointerPath) 
        
        
        self.pixMapItem = QtWidgets.QGraphicsPixmapItem(self.pixmap)
        
        self.pixMapItem.setOffset(9.5*0.77*(self.RectX),(self.RectY/8.0)) #### Positioning pointer
        self.pixMapItem.setScale(0.13) #### Pointer scaling
        
        self.scene.addItem(self.pixMapItem)

        self.pen = QtGui.QPen()
        
        self.pen.setColor (self.tickColor) 
        
        
        for i in range(1,10):
            self.tickHeight = 3.0 #### Tick height
            self.pen.setWidth(5) #### Tick width
            
            if i == 5:
                self.tickHeight =10.0 #### Zero tick height
                self.pen.setWidth(3) #### Zero tick width
            
            self.lineItem = QtWidgets.QGraphicsLineItem()
            self.lineItem.setToolTip(str(i))
            self.lineItem.setLine((i/10.0)*self.RectX,(self.RectY/2.0)-self.tickHeight,(i/10.0)*self.RectX,(self.RectY/2.0)+self.tickHeight)
            self.scene.addItem(self.lineItem)
            self.lineItem.setPen(self.pen)
            
    def update_graphicsView(self,incr=None):
        #### Updating pointer
        if incr == None:
            incr=0.0
        incr =  ((incr+100.0)/20.0)-0.5
        self.pixMapItem.setOffset(incr*0.77*(self.RectX),(self.RectY/8.0))



